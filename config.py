import re
import json
import os,sys

try:
    with open('config.json') as config_file:
        data = json.load(config_file)
except BaseException as e:
    print("config file not available")
    sys.exit()


router = data["router"]["rrc00"]

baseURL = data["baseURL"]

start_date = data["start_date"]

end_date = data["end_date"]

log_file = router+data["log_file"]

file_pattern = re.compile(data["file_pattern"])


try:
    if not os.path.exists(log_file):
        print("logfile not present, created:",log_file)
        open(log_file, 'a+')
except BaseException as e:
    print(str(e))
    sys.exit()  
