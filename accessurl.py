import requests
import re
import os,sys
from html.parser import HTMLParser
import config


#TableParser Code modified from: https://docs.python.org/3/library/html.parser.html
class TableParser(HTMLParser):
     
     def __init__(self):
         HTMLParser.__init__(self)
         #initialize parser to false meaning outside the tag <td>
         self.in_td = False
         #Temporary list to store filename and size
         self.list=[]
     
     #inside td, set to True
     def handle_starttag(self, tag, attrs):
         if tag == 'td':
             self.in_td = True

     #Append the data thats inside tag td
     def handle_data(self, data):        
         if self.in_td: 
             self.list.append(data)   

     #To indicate outside td
     def handle_endtag(self, tag):
         self.in_td = False


#To access the source page of the site
def urlaccess(sites):
    try:
        filelist=[]
        #for each url in the list of sites 
        for url in sites:
            #requests the page source of the url
            r = requests.get(url)
            #stores the whole page 
            page_source = r.text
            #splits the lines on the basis of newline
            page_source = page_source.split('\n')
            i=0

            #for each line in source page, search for the filename that with matches the filepattern 
            for row in page_source[:]:
                file_pattern = config.file_pattern
                p = TableParser()
                if re.search(file_pattern,row):
                    #append the filename and its size in the filelist
                    p.feed(row)
                    filelist.append((p.list[0],p.list[2]))   
            i=i+1
        return filelist
    except BaseException as e:
        print("URL meta info access error",e)
        sys.exit()

            
