from datetime import datetime, timedelta
import sys,os
from datetime import date
import logging
import config
import accessurl
import urllib.request
import dirCreate
import latestdate
import fileinput
import calendar

#importing the requirements from the config file
start = config.start_date
end = config.end_date
today = str(date.today())
router = config.router
log_file = config.log_file 


def clean_log(stripline):
    with open(log_file,'r+') as f:
        #read the log file and store in filedata
        filedata = f.read()
        #when f.read points to eof, the pointer is pointd to zeroth item of the file
        f.seek(0)
        #replacing the stripline(the line that needs to be stripped) with "\n"
        filedata = filedata.replace(stripline,'\n')
        #clean the logfile
        f.truncate(0)
        f.write(filedata)
    
'''
def clean_log(stripline):
    with open(log_file, 'r+') as f:
        t = f.read()
        f.seek(0)
        for line in t.split("\n"):
            if not stripline in line:
                f.write(line + "\n")
        f.truncate()
'''

# The following function (remove_empty_lines) is modified from 
# https://stackoverflow.com/questions/53358985/remove-n-with-strip-in-python
 
def remove_empty_lines():
    with open(log_file) as filehandle:
        lines = filehandle.readlines()
    #stripping the \n 
    with open(log_file, 'w') as filehandle:
        lines = filter(lambda x: x.strip(), lines)
        filehandle.writelines(lines)   


#to check if the file present in the log 
def filecheck(file):
    try:
        #open log file
        with open(log_file, 'r') as read_obj:
                for line in read_obj: 
                    #if the filename found               
                    if file[0] in line:
                        #check for the size
                        memFile = line.split(" ")
                        sizeFile = memFile[1]
                        lenString = len(sizeFile)
                        memFileSize = int(sizeFile[0:lenString-2])
                        siteFileSize = file[1]
                        lenSite = len(siteFileSize)
                        webFileSize = int(siteFileSize[0:lenSite-1])
                        #if size of the file present in web repository is bigger
                        #then download is required
                        if webFileSize > memFileSize:
                            #print("download required")
                            #print(memFile,webFileSize,memFileSize)
                            read_obj.close() 
                            clean_log(line)
                            return True
                        else:
                            #print("no download required")  
                            return False
                #print("download required")  
                return True
        read_obj.close()
    except BaseException as e:
        print(str(e))            


def urlDownload(start,end):
    
    try:
        startDate = datetime.strptime(start,"%Y-%m-%d")
        endDate = datetime.strptime(end, "%Y-%m-%d")
        todayDate = datetime.strptime(today, "%Y-%m-%d")
    except BaseException as e:
        print("Date error:",str(e))
        sys.exit()
    #print(startDate,endDate)
    startYear = datetime.strftime(startDate, "%Y")
    startMonth = datetime.strftime(startDate,"%m")
    startDay = datetime.strftime(startDate,"%d")

    endYear = datetime.strftime(endDate, "%Y")
    endMonth = datetime.strftime(endDate,"%m")
    endDay = datetime.strftime(endDate,"%d") 

    if (startDate > endDate) or (startDate > todayDate) or (endDate > todayDate):
        print('Invalid dates or Future dates')
        sys.exit()
    else: 
        num_months = (endDate.year - startDate.year) * 12 + (endDate.month - startDate.month)
        
        if num_months > 0 : 
            
            # If days range over months

            # This is done in three stages:
            # stage 1: first month number of days data 
            # stage 2: any number of months data, starting from 2  month to last but
            #         one month
            # stage 3: last month's number of days data

            #####################################
            # stage 1: first month's days data  #
            #####################################
            dayRange = calendar.monthrange(startDate.year,startDate.month)
            #print(dayRange[1])
            
            newEndDatestring = startYear+"-"+startMonth+"-"+str(dayRange[1])
            newEndDate = datetime.strptime(newEndDatestring,"%Y-%m-%d")
            #print(newstartDate)
            span = newEndDate - startDate
            #print("span")
            url = config.baseURL + router + "/" + startYear + "." + startMonth 
            filelist = accessurl.urlaccess([url])
            #print(filelist)
            if len(filelist) == 0:
                    print("No data available",url)
                    
            newfilelist=[]
            for i in range (span.days + 1):
                day = startDate + timedelta(days=i)
                daystring = str(day)
                daydata = daystring.split("-")
                dayinfo = daydata[0]+daydata[1]+daydata[2][0:2]
                #print(dayinfo)
                for j in filelist:
                    if j[0].find(dayinfo) > 0:
                        newfilelist.append(j)

            for i in newfilelist:
                downloadRequired = filecheck(i)
                if downloadRequired == True:
                    #print("download required",i[0])
                    downloadURL = url+"/"+i[0] 
                    #print(downloadURL)
                    fileLoc = dirCreate.create(router,startYear,startMonth)
                    #print(fileLoc)
                    #print("downloading")
                    urllib.request.urlretrieve(downloadURL, fileLoc+"/"+i[0])
                    logString = i[0]+" "+i[1]+"\n"
                    logHandle = open(log_file,'a+')
                    logHandle.write(logString)
                    logHandle.close()
            
            
            intstartMonth = int(startMonth)+1
            if intstartMonth > 12:
                intstartMonth = 1
                intstartYear = int(startYear)+1
                startYear=str(intstartYear)
            if intstartMonth < 10:
                startMonth = "0"+str(intstartMonth)
            else:
                startMonth = str(intstartMonth)

            #####################################
            # stage 2: number of months data    #
            #####################################
            for j in range(1,num_months):
                #print("Success, form the URI",num_months)
                url = config.baseURL+router+"/"+ startYear + "." + startMonth 
                #print(url)
                filelist = accessurl.urlaccess([url])
                #print(filelist)
                if len(filelist) == 0:
                    print("No data available",url)
                    
                for i in filelist:
                    downloadRequired = filecheck(i)
                    if downloadRequired == True:
                        #print("download required",i[0])
                        downloadURL = url+"/"+i[0] 
                        #print(downloadURL)
                        fileLoc = dirCreate.create(router,startYear,startMonth)
                        #print(fileLoc)
                        #print("downloading")
                        urllib.request.urlretrieve(downloadURL, fileLoc+"/"+i[0])
                        logString = i[0]+" "+i[1]+"\n"
                        logHandle = open(log_file,'a+')
                        logHandle.write(logString)
                        logHandle.close()
                intstartMonth = int(startMonth)+1
                if intstartMonth > 12:
                    intstartMonth = 1
                    intstartYear = int(startYear)+1
                    startYear=str(intstartYear)
                if intstartMonth < 10:
                    startMonth = "0"+str(intstartMonth)
                else:
                    startMonth = str(intstartMonth)

            #####################################
            # stage 3: last month's days data   #
            #####################################
            newstartDatestring = startYear+"-"+startMonth+"-"+"01"
            newstartDate = datetime.strptime(newstartDatestring,"%Y-%m-%d")
            #print(newstartDate)
            span = endDate - newstartDate
            url = config.baseURL + router + "/" + startYear + "." + startMonth 
            filelist = accessurl.urlaccess([url])
            #print(filelist)
            if len(filelist) == 0:
                    print("No data available",url)
                    
            newfilelist=[]
            for i in range (span.days + 1):
                day = newstartDate + timedelta(days=i)
                daystring = str(day)
                daydata = daystring.split("-")
                dayinfo = daydata[0]+daydata[1]+daydata[2][0:2]
                #print(dayinfo)
                for j in filelist:
                    if j[0].find(dayinfo) > 0:
                        newfilelist.append(j)

            for i in newfilelist:
                downloadRequired = filecheck(i)
                if downloadRequired == True:
                    #print("download required",i[0])
                    downloadURL = url+"/"+i[0] 
                    #print(downloadURL)
                    fileLoc = dirCreate.create(router,startYear,startMonth)
                    #print(fileLoc)
                    #print("downloading")
                    urllib.request.urlretrieve(downloadURL, fileLoc+"/"+i[0])
                    logString = i[0]+" "+i[1]+"\n"
                    logHandle = open(log_file,'a+')
                    logHandle.write(logString)
                    logHandle.close()
        else:
            # if days range in a single month
            # to do: function
            span = endDate - startDate
            url = config.baseURL+router+"/"+ startYear + "." + startMonth 
            #print(url)
            filelist = accessurl.urlaccess([url])
            #print(filelist)
            if len(filelist) == 0:
                    print("No data available",url)
            newfilelist=[]
            for i in range (span.days + 1):
                day = startDate + timedelta(days=i)
                daystring = str(day)
                daydata = daystring.split("-")
                dayinfo = daydata[0]+daydata[1]+daydata[2][0:2]
                for j in filelist:
                    if j[0].find(dayinfo) > 0:
                        newfilelist.append(j)
            for i in newfilelist:
                downloadRequired = filecheck(i)
                if downloadRequired == True:
                    #print("download required")
                    downloadURL = url+"/"+i[0] 
                    #print(downloadURL)
                    fileLoc = dirCreate.create(router,startYear,startMonth)
                    #print(fileLoc)
                    #print("downloading")
                    urllib.request.urlretrieve(downloadURL, fileLoc+"/"+i[0])   
                    logString = i[0]+" "+i[1]+"\n"
                    logHandle = open(log_file,'a+')
                    logHandle.write(logString)
                    logHandle.close()
    


#if start and end dates are specified
#download from start till end date specified
if start != "" and end != "":
    urlDownload(start,end)
#if start date is not mentioned but end date is mentioned
#retreive the latest date
#start date == latest date
#download from start date to end date specified 
elif start == "" and end != "":
    startdate = latestdate.recent()
    print(startdate)
    if startdate == "empty":
        month = date.today().month;
        year = date.today().year
        if month < 10:
            month = "0"+str(month) 
        startdate = str(year) + "-"+month+"-"+"01" 
        urlDownload(startdate,end)
    else:
        urlDownload(startdate[0],end)

#if end date is not mentioned, download from the given end date
#till current date
elif end == "" and start != "":
    #print("No end date")
    enddate = str(today)
    urlDownload(start,enddate)
    
else:
    #default case
    #print("No arguments")
    startdate = latestdate.recent()
    enddate = str(today)
    if startdate == "empty":
        month = date.today().month;
        year = date.today().year
        if month < 10:
            month = "0"+str(month) 
        startdate = str(year) + "-"+month+"-"+"01" 
        urlDownload(startdate,enddate)
    else:
        urlDownload(startdate[0],enddate)

remove_empty_lines()
print("Download Complete,exiting....")
