import re
import datetime
import config
import sys

places = []

log_file = config.log_file

try:
    with open(log_file, 'r') as filehandle:
        for line in filehandle:
            places.append(line)
except BaseException as e:
    print(str(e))

date_pattern = config.file_pattern



def get_date(filename):
    matched = date_pattern.search(filename)
    if not matched:
        return None   
    file = filename.split(".")
    y = file[1][0:4]
    m= file[1][4:6]
    d= file[1][6:8]
    return datetime.date(int(y), int(m), int(d))

def recent():
    try:   
        dates = (get_date(fn) for fn in places)
        dates = (d for d in dates if d is not None)
        last_date = max(dates)
        last_dateStr=str(last_date)
        last_dateSplit=last_dateStr.split("-")
        dateMax = last_dateSplit[0]+last_dateSplit[1]+last_dateSplit[2] 
        filelist=[]
        for i in places:
            j = i.find(dateMax)
            if j > 0:
                f = i.split(' ')
                filelist.append(f[0])
        maxTime=0
        for i in filelist:
            splitname = i.split('.')
            if int(splitname[2]) >= maxTime:
                maxTime = int(splitname[2])
        
        return([last_dateStr,maxTime])
    except BaseException as e:
        print(log_file, "empty, default download of present month data")
        return "empty"
        #sys.exit()