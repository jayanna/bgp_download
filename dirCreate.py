import time, datetime, os
from datetime import datetime, timedelta
import config
import sys



def create(router,year,month):
    try:
        if not os.path.exists(router):
            os.mkdir(router)
            routerYear = os.path.join(router,year)
            os.makedirs(routerYear)
            os.makedirs(os.path.join(routerYear,month))
            return os.path.join(routerYear,month)
        else:
            if not os.path.exists(os.path.join(router,year)):
                os.makedirs(os.path.join(router,year))
                routerYear = os.path.join(router,year)
                os.makedirs(os.path.join(routerYear,month))
                return os.path.join(routerYear,month)
            else:
                routerYear = os.path.join(router,year)
                if not os.path.exists(os.path.join(routerYear,month)):
                    os.makedirs(os.path.join(routerYear, month))
                    return os.path.join(routerYear, month)
                else:
                    return os.path.join(routerYear, month)
    except BaseException as e:
        print(str(e))
        sys.exit()